// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import 'zone.js/dist/zone-testing';
import { getTestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';
import { NgModule } from '@angular/core';
import {AuthService} from './app/services/auth.service';
import {LocalStorageAuthService} from './app/services/localStorage.auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

declare const require: {
  context(path: string, deep?: boolean, filter?: RegExp): {
    keys(): string[];
    <T>(id: string): T;
  };
};

@NgModule({
  providers: [
    {
      provide: AuthService,
      useValue: new LocalStorageAuthService()
    }
  ],
  imports: [HttpClientTestingModule]
})
class TestModule {}

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  [BrowserDynamicTestingModule, TestModule],
  platformBrowserDynamicTesting()
);
// Then we find all the tests.
const context = require.context('./', true, /\.spec\.ts$/);
// And load the modules.
context.keys().map(context);
