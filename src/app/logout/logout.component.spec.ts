import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoutComponent } from './logout.component';
import { AuthService } from '../services/auth.service';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let authServiceStub: Partial<AuthService>;
  let authService: AuthService;

  beforeEach(async () => {
    authServiceStub = {
      isLogged: () => true,
      getUserEmail: () => 'batman@ehlabs.com',
      logout: function noop() {}
    };

    await TestBed.configureTestingModule({
      declarations: [ LogoutComponent ],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    })
    .compileComponents();

    authService = TestBed.inject(AuthService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
