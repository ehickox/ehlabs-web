import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-topmenu',
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.css']
})
export class TopmenuComponent implements OnInit {
  menu: any; // TODO: do this the right way and don't use any

  @Input() presentationMode: 'ribbon' | 'stack' = 'ribbon'

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}
