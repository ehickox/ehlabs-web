import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopmenuComponent } from './topmenu.component';
import { AuthService } from '../services/auth.service';
import { AppModule } from '../app.module';

describe('TopmenuComponent', () => {
  let component: TopmenuComponent;
  let fixture: ComponentFixture<TopmenuComponent>;
  let authServiceStub: Partial<AuthService>;
  let authService: AuthService;

  beforeEach(async () => {
    authServiceStub = {
      isLogged: () => true,
      getUserEmail: () => 'batman@ehlabs.com'
    };

    await TestBed.configureTestingModule({
      declarations: [ TopmenuComponent ],
      providers: [{ provide: AuthService, useValue: authServiceStub }],
      imports: [ AppModule ]
    })
    .compileComponents();
    authService = TestBed.inject(AuthService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
