import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './post/post-list.component';
import { PostViewComponent } from './post/post-view.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ComposePostComponent } from './post/compose-post.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TosComponent } from './tos/tos.component';
import { ChatComponent } from './chat/chat.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'blog', component: PostListComponent },
  { path: 'blog/:username', component: PostListComponent },
  { path: 'blog/post/:postId', component: PostViewComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'compose', component: ComposePostComponent },
  { path: 'about', component: AboutComponent },
  { path: 'tos', component: TosComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'chat/:username', component: ChatComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
