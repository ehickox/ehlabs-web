import { User } from './user.model';

describe('User Model', () => {
  it('converts server response to a model instance', () => {
    const sampleJson = {
      "DEFAULT_API_TOKEN_EXPIRATION_SECS": 3600,
      "btn_class": "btn btn-default",
      "chat_url": "chat/u/batman",
      "email": "batman@ehlabs.com",
      "id": 42,
      "is_admin": false,
      "last_ip": "172.217.10.78",
      "lmod": "2020-12-12 22:26:55.906603",
      "name": "batman",
      "registered_on": "2020-07-10 04:25:28.984729",
      "timestamp": 1594355128984,
      "unread_count": 7
    };

    const user = new User(sampleJson);
    expect(user.email).toEqual('batman@ehlabs.com');
    expect(user.id).toEqual(42);
    expect(user.isAdmin).toEqual(false);
    expect(user.name).toEqual('batman');
    expect(user.unreadCount).toEqual(7);
    expect([...Object.keys(user)].length).toEqual(5);
  });
});
