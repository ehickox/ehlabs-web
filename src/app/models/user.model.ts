export class User {
  email = '';
  id = NaN;
  isAdmin = false;
  name = '';
  unreadCount = NaN;

  constructor(data: any) {
    if (!data) {
      return;
    }
    if (typeof data.email === 'string') {
      this.email = data.email;
    }
    if (typeof data.id === 'number') {
      this.id = data.id;
    }
    if (typeof data.id_admin === 'boolean') {
      this.isAdmin = data.is_admin;
    }
    if (typeof data.name === 'string') {
      this.name = data.name;
    }
    if (typeof data.unread_count === 'number') {
      this.unreadCount = data.unread_count;
    }
  }
}
