import { Post } from './post.model';

describe('Post model', () => {
  describe(`#new Post(sampleJson)`, () => {
    it('a new post initialized from sample json data', () => {
      const sampleJson = {
        "body": "seems to be running fine",
        "date": "2020-12-04 07:52:27.955619",
        "html_ipfs_hash": "QmPsUXV2c553XpDc7wfiQc9FT9VgNTX2CteQaV8k3kXfn3",
        "id": 14,
        "ipfs_hash": "QmakzA4kurF7QwcWHzEscFwggzh1u46Qnt2kNsXz1zGjhv",
        "ipfs_url": "https://ipfs.io/ipfs/QmPsUXV2c553XpDc7wfiQc9FT9VgNTX2CteQaV8k3kXfn3",
        "json": "{\"body\": \"seems to be running fine\", \"timestamp\": 1607068347955, \"date\": \"2020-12-04 07:52:27.955619\", \"ipfs_hash\": \"\", \"json\": {}, \"owner_id\": 1, \"tagline\": \"\", \"tags\": [], \"title\": \"test ehlabs-web\"}",
        "link": "https://www.ehlabs.net/blog/14",
        "owner_id": 1,
        "owner_name": "eli",
        "paras": ["seems to be running fine"],
        "tags": [],
        "timestamp": 1607068347955,
        "title": "test ehlabs-web",
        "view_url": "https://www.ehlabs.net/blog/14"
      }

      const actual = new Post(sampleJson);
      expect(actual.title).toEqual('test ehlabs-web')
      expect(actual.body).toEqual('seems to be running fine');
      expect(actual.ownerId).toEqual(1);
      expect(actual.ownerName).toEqual('eli');
      expect(actual.date).toEqual(1607068347955);

      expect([...Object.keys(actual)].sort()).toEqual([
        'title',
        'body',
        'ownerId',
        'ownerName',
        'date'
      ].sort());
    });
  });
});

