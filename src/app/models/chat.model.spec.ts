import {Chat} from './chat.model';

describe('Chat model', () => {
  describe('constructor', () => {
    describe('#constructor(extraProp)', () => {
      it('ignores extra prop', () => {
        const extraProp = {
          "body": "hai",
          "id": 77,
          "name": "zach",
          "millis": 1607195895000,
          "sender_id": 4,
          "timestamp": "2020-12-05 19:18:15",
          "extra_prop": true
        }

        const actual = new Chat(extraProp)

        expect(actual.body).toEqual('hai');
        expect(actual.id).toEqual(77);
        expect(actual.senderName).toEqual('zach');
        expect(actual.senderId).toEqual(4);
        expect(actual.millis).toEqual(1607195895000);
        expect([...Object.keys(actual)]).toEqual(['body', 'id', 'senderName', 'senderId', 'timestamp']);
      });
    });
  });
});
