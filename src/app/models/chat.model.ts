export class Chat {
  public body = '';
  public id = NaN;
  public senderName = '';
  public senderId = NaN;
  public millis = NaN;

  constructor(data?: any) {
    if (!data) {
      return;
    }

    if (typeof data.body === 'string') {
      this.body = data.body;
    }
    if (typeof data.id === 'number') {
      this.id = data.id;
    }
    if (typeof data.name === 'string') {
      this.senderName = data.name;
    }
    if (typeof data.sender_id === 'number') {
      this.senderId = data.sender_id;
    }
    if (typeof data.millis === 'number') {
      this.millis = data.millis;
    }
  }
}
