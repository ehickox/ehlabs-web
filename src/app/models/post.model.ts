export class Post {
  public title = '';
  public body = '';
  public ownerId = NaN;
  public ownerName = '';

  // represents milliseconds since 1 January 1970 UTC, so we can align with native Date object
  public date = NaN;

  constructor(data?: any) {
    if (!data) {
      return;
    }

    if (typeof data.title === "string") {
      this.title = data.title;
    }
    if (typeof data.body === "string") {
      this.body = data.body;
    }
    if (typeof data.timestamp === "number") {
      this.date = data.timestamp;
    }
    if (typeof data.owner_id === "number") {
      this.ownerId = data.owner_id;
    }
    if (typeof data.owner_name === "string") {
      this.ownerName = data.owner_name;
    }
  }

}

