import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm!: NgForm;
  loginError = ""
  
  constructor(public authService: AuthService) { 
  }

  ngOnInit(): void {
  }

  async sumbitLogin() {
    try {
      await this.authService.login(this.loginForm.value.email, this.loginForm.value.pass);
      this.loginError = '';
      this.loginForm.resetForm();
    } catch(e) {
      this.loginError = `${e?.message}`;
    }
  }
}
