import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthService } from '../services/auth.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authServiceStub: Partial<AuthService>;
  let authService: AuthService;

  beforeEach(async () => {
    authServiceStub = {
      isLogged: () => true,
      getUserEmail: () => 'batman@ehlabs.com',
      logout: function noop() {}
    };

    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    })
    .compileComponents();

    authService = TestBed.inject(AuthService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
