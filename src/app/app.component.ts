import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ehlabs-web';

  isMobileHeaderVisible = false;

  showModal(): void {
    this.isMobileHeaderVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isMobileHeaderVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isMobileHeaderVisible = false;
  }
}
