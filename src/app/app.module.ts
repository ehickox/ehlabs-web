import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostCardComponent } from './post/post-card.component';
import { PostViewComponent } from './post/post-view.component';
import { PostListComponent } from './post/post-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { LoginComponent } from './login/login.component';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { ComposePostComponent } from './post/compose-post.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TosComponent } from './tos/tos.component';
import { ChatComponent } from './chat/chat.component';
import { ChatCardComponent } from './chat/chat.card.component'
import { ChatListComponent } from './chat/chat.list.component';
import { ComposeChatComponent } from './chat/compose-chat.component';
import { ChatMenuComponent } from './chat/chat-menu.component';
import { IconDefinition } from '@ant-design/icons-angular';

import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzIconModule } from 'ng-zorro-antd/icon';

// Ant's official documentation recomends not to load all the icons. Instead, we should only load
// the icons we are using. Loading them all anyways for convenience.
//  https://ng.ant.design/components/icon/en#static-loading-and-dynamic-loading
import * as AllIcons from '@ant-design/icons-angular/icons';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    PostCardComponent,
    PostListComponent,
    PostViewComponent,
    LoginComponent,
    TopmenuComponent,
    ComposePostComponent,
    HomeComponent,
    AboutComponent,
    TosComponent,
    ChatComponent,
    ChatCardComponent,
    ChatListComponent,
    ComposeChatComponent,
    ChatMenuComponent,
    ChatMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    CommonModule,
    NzCardModule,
    NzListModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzFormModule,
    NzDropDownModule,
    NzMenuModule,
    NzInputModule,
    NzSpaceModule,
    NzModalModule,
    NzIconModule.forRoot(icons)
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
