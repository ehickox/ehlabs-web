import { Component, Input } from '@angular/core';
import { Chat } from '../models/chat.model';
import { ChatMode } from './chat-mode';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat.list.component.html',
  styleUrls: ['./chat.list.component.css']
})
export class ChatListComponent {
  @Input() chatMode!: ChatMode;

  @Input() errorMessage = '';
  @Input() isLoading = false;
  @Input() chats: Chat[] = [];

  constructor() {
  }

}
