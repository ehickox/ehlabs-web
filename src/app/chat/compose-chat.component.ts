import { Input, Component, ViewChild } from '@angular/core';
import { Chat } from '../models/chat.model';
import { ChatService } from '../services/chat.service';
import { NgForm } from '@angular/forms';
import { ChatMode } from './chat-mode';

@Component({
  selector: 'app-compose-chat',
  templateUrl: './compose-chat.component.html',
  styleUrls: ['./compose-chat.component.css']
})
export class ComposeChatComponent {
  @Input() chatMode!: ChatMode;

  message = '';
  errorText = '';
  @ViewChild(NgForm) form!: NgForm;

  constructor(private chatService: ChatService) { }

  async sendChat() {
    try {
      if (this.chatMode.type === 'global') {
        await this.chatService.sendGlobalChat(this.message);
      } else {
        await this.chatService.sendDirectChat(this.chatMode.identifier, this.message);
      }

      this.message = '';
      this.errorText = '';
      this.form.resetForm();
    }
    catch (e) {
      this.errorText = `${e?.message}`;
    }
  }

  getFormTitle(): string {
    switch (this.chatMode.type) {
      case 'direct':
        return `Send a Message to ${this.chatMode.identifier}`;
      case 'global':
        return 'Send a Message to Everyone';
      default:
        return 'Send a Message';
    }
  }
}
