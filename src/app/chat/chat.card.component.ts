import { Component, OnInit, Input } from '@angular/core';
import { Chat } from '../models/chat.model';

@Component({
  selector: 'app-chat-card',
  templateUrl: './chat.card.component.html',
  styleUrls: ['./chat.card.component.css']
})
export class ChatCardComponent implements OnInit {
  @Input() chat: Chat = new Chat();
  chatJson = '';

  constructor() { }

  ngOnInit(): void {
    this.chatJson = JSON.stringify(this.chat, null, 2);
  }

}
