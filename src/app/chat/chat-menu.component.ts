import { Input, Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'app-chat-menu',
  templateUrl: './chat-menu.component.html',
  styleUrls: ['./chat-menu.component.css']
})
export class ChatMenuComponent implements OnInit {
  @Input() users: User[] = [];

  constructor() {}

  ngOnInit(): void {
  }

}
