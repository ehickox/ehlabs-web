import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChatComponent } from './chat.component';
import { ChatService } from '../services/chat.service';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;
  let chatServiceStub: Partial<ChatService>;

  beforeEach(async () => {
    chatServiceStub = {
      fetchGlobalChats: () => Promise.reject('not logged in')
    }

    await TestBed.configureTestingModule({
      declarations: [ ChatComponent ],
      providers: [{ provide: ChatService, useValue: chatServiceStub }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
