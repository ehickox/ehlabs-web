import { Component } from '@angular/core';
import { Router, ActivatedRoute, ActivationEnd } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ChatMode } from './chat-mode';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Chat } from '../models/chat.model';
import { Subscription } from 'rxjs';
import { ChatService } from '../services/chat.service';

function compareChats(chat: Chat, other: Chat) {
  return other.millis - chat.millis;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent {
  chatMode: ChatMode | null = null;
  message = '';

  users: User[] = [];
  isLoadingUsers = false;

  routerSubscription: Subscription;

  chatSubscription: Subscription | null = null;
  isLoadingChats = false;
  chatErrorMessage = '';

  // List of displayed chats ordered by time.
  chats: Chat[] = [];
  // Lookup if a chat exists in `this.chats`. Evicting chats has not been implemented.
  chatsMembership = new Map<number, true>();

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private chatService: ChatService
  ) {
    console.info('chat constructor');


    this.loadUsers();

    this.routerSubscription = this.router.events.subscribe((e) => {
      console.info('router event', e);
      if (e instanceof ActivationEnd) {
        this.chatMode = this.getChatMode();
        this.loadChats();
      }
    });
  }

  public isLogged = () => this.authService.isLogged();

  private getChatMode(): ChatMode {
      const userName = this.route.snapshot.paramMap.get('username') || '';

      if (userName) {
        return { type: 'direct', identifier: userName };
      }
      return { type: 'global' };
  }

  private async loadUsers() {
    try {
      console.info('loading users...');
      this.users = [];
      this.isLoadingUsers = true;
      this.users = await this.userService.fetchUsers();
      this.message = '';
    } catch(e) {
      this.message = `${e?.message}`;
    } finally {
      this.isLoadingUsers = false;
    }
  }

  private async loadChats() {
    console.info('loading chats...');
    this.chats = [];
    this.isLoadingChats = true;

    if (this.chatSubscription) {
      this.chatSubscription.unsubscribe();
    }
    if (!this.chatMode) {
      return
    }

    const observable = this.chatService.fetchAndSubscribeToChats(this.chatMode);
    this.chatSubscription = observable.subscribe(({ error, chats }) => {
      console.info('got some chats', chats, error);
      this.isLoadingChats = false;
      this.chatErrorMessage = error?.message || '';

      for (const chat of chats) {
        if (!this.chatsMembership.has(chat.id)) {
          this.chats.push(chat);
          this.chatsMembership.set(chat.id, true);
        }
      }
      this.chats.sort(compareChats);
    });
  }

  ngOnDestroy() {
    if (this.chatSubscription) {
      this.chatSubscription.unsubscribe();
    }
    this.routerSubscription.unsubscribe();
  }
}
