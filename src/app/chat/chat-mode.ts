export type ChatMode = {
  type: 'direct'
  identifier: string
} | {
  type: 'global'
}

