import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChatService } from '../services/chat.service';

import { ChatListComponent } from './chat.list.component';

describe('Chat.ListComponent', () => {
  let component: ChatListComponent;
  let fixture: ComponentFixture<ChatListComponent>;
  let chatServiceStub: Partial<ChatService>;

  beforeEach(async () => {
    chatServiceStub = {
      fetchGlobalChats: () => Promise.reject('not logged in')
    }
    await TestBed.configureTestingModule({
      declarations: [ ChatListComponent ],
      providers: [{ provide: ChatService, useValue: chatServiceStub }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
