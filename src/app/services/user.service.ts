import { Injectable } from '@angular/core';
import {User} from '../models/user.model';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';

export interface IUserService {
  fetchUsers: () => Promise<User[]>
}

@Injectable({
  providedIn: 'root'
})
export class UserService implements IUserService {

  constructor(private authService: AuthService, private http: HttpClient) {}

  public async fetchUsers(): Promise<User[]> {
    const headers = {
      Authorization: this.authService.getAuthHeader()
    };
    const data = await this.http.get('/api/v1/users', { headers }).toPromise();

    if (!Array.isArray(data)) {
      return [];
    }
    return data.map(x => new User(x));
  }
}
