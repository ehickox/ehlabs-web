import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface IAuthService {
  login: (email: string, token: string) => Promise<void>
  logout: () => void
  isLogged: () => boolean
  getUserEmail: () => string | null
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuthService {

  constructor(private http: HttpClient) {
    try {
      if (localStorage.getItem('ehlabs-login')) {
        this.sessionInfo = JSON.parse(localStorage.getItem('ehlabs-login')!);
      }
    }
    catch (e) {
      console.error(e);
      localStorage.removeItem('ehlabs-login');
    }
  }

  private sessionInfo: { email: string, token: string } | null  = null

  async login(email: string, pass: string) {
    const headers =  {
      Authorization: `Basic: ${btoa(email + ':' + pass)}`
    };
    const json = await this.http.get(`/api/v1/auth/token`, { headers }).toPromise() as any;

    if (!json || !json.token || (typeof json.token !== 'string')) {
      throw new Error(`Auth failed; missing token`);
    }

    this.sessionInfo = { email, token: json.token }

    // TODO: Store tokens in a cookie. It is not secure to use localStorage
    localStorage['ehlabs-login'] = JSON.stringify(this.sessionInfo);
  }

  async check() {
    if (!this.sessionInfo) {
      throw new Error('not logged in');
    }

    const headers =  {
      Authorization: this.getAuthHeader()
    };
    const json = await this.http.get(`/api/v1/auth/check`, { headers }).toPromise() as any;

    return json;
  }

  isLogged() { return !!this.sessionInfo }
  getUserEmail() { return this.sessionInfo && this.sessionInfo.email }

  getAuthHeader() {
    if (!this.sessionInfo) {
      throw new Error('not logged in');
    }
    return `Basic: ${btoa(this.sessionInfo.email + ':' + this.sessionInfo.token)}`;
  }


  logout() {
    this.sessionInfo = null;
    localStorage.removeItem('ehlabs-login');
  }
}
