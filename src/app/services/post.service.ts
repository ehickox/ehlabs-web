import { inject, Injectable, InjectionToken } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post.model';
import { LocalStoragePostService } from './localStorage.post.service';

class PostService implements IPostService {
  constructor(private authService: AuthService, private http: HttpClient) { }

  async fetchFeed(): Promise<Post[]> {
    const responseData = await this.http.get('/api/v1/blog/feed').toPromise();

    return this.validateFeedData(responseData);
  }

  async fetchUserFeed(username: string): Promise<Post[]> {
    const responseData = await this.http.get(`/api/v1/blog/u/${username}`).toPromise();

    return this.validateFeedData(responseData);
  }

  async fetchPostById(id: string): Promise<Post | null> {
    const responseData = await this.http.get(`/api/v1/blog/${id}`).toPromise();

    return new Post(responseData);
  }

  private validateFeedData(feed: unknown): Post[] {
    if (feed === null || feed === undefined) {
      return [];
    }
    if (!Array.isArray(feed)) {
      throw new Error(`Unexpected state, expected feed to be an array or null, but it is type: $(typeof feed)`);
    }

    return feed.map(data => new Post(data));
  }

  async createPost(post: Post): Promise<void> {
    const headers =  {
      Authorization: this.authService.getAuthHeader()
    };
    await this.http.post(`/api/v1/blog`, post, { headers }).toPromise();
  }
}

export interface IPostService {
  fetchFeed: () => Promise<Post[]>,
  fetchUserFeed: (username: string) =>Promise<Post[]>,
  fetchPostById: (id: string) =>Promise<Post | null>,
  createPost: (post: Post) =>Promise<void>
}

export const POST_SERVICE_TOKEN = new InjectionToken<IPostService>('PostService', {
  providedIn: 'root',
  //factory: () => new LocalStoragePostService(inject(AuthService), inject(HttpClient))
  factory: () => new PostService(inject(AuthService), inject(HttpClient))
});
