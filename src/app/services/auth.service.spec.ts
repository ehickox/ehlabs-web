import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

let httpClientSpy: { get: jasmine.Spy };

describe('AuthService', () => {
  let service: AuthService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AuthService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('logging in with valid creds', () => {
    it('returns undefined, and saves email and token to local storage', async (done) => {
      const res = service.login('batman@example.com', 'abc123').then((res) => {
        expect(res).toEqual(undefined);
        expect(localStorage.getItem('ehlabs-login')).toEqual(JSON.stringify({ email: 'batman@example.com', token: 'fake-token' }));
        done();
      }, () => {
        expect(false).toEqual(true);
      });

      const req = httpTestingController.expectOne('/api/v1/auth/token');

      req.flush({ token: 'fake-token' });
    });

    beforeEach(() => {
      localStorage.clear();
    });

    afterEach(() => {
      localStorage.clear();
    });
  });

  describe('loging in with valid creds, then logging out', () => {
    beforeEach(() => {
      localStorage.clear();
    });

    afterEach(() => {
      localStorage.clear();
    });

    it('sets ehlabs-login in localStorage, then removes it from local storage', async (done) => {
      const res = service.login('batman@example.com', 'abc123').then((res) => {
        expect(localStorage.getItem('ehlabs-login')).toEqual(JSON.stringify({ email: 'batman@example.com', token: 'fake-token' }));

        service.logout();
        expect(localStorage.length).toEqual(0);

        done();
      }, () => {
        expect(false).toEqual(true);
      });

      const req = httpTestingController.expectOne('/api/v1/auth/token');

      req.flush({ token: 'fake-token' });
    });
  });
});
