import { TestBed } from '@angular/core/testing';
import { IPostService, POST_SERVICE_TOKEN } from './post.service';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Post } from '../models/post.model';

describe('PostService', () => {
  let service: IPostService;
  let authServiceStub: Partial<AuthService>;
  let authService: AuthService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    authServiceStub = { isLogged: () => false };

    TestBed.configureTestingModule({
      providers: [{ provide: AuthService, useValue: authServiceStub }],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(POST_SERVICE_TOKEN);
    authService = TestBed.inject(AuthService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
