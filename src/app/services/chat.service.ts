import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chat } from '../models/chat.model';
import { AuthService } from '../services/auth.service';
import { of, interval, Observable, timer } from 'rxjs';
import { startWith, flatMap, map, catchError } from 'rxjs/operators';
import { ChatMode } from '../chat/chat-mode';

// the server throttles chat creations every 2 seconds.
// there is no throttle for GETs
const POLL_MILLIS = 700;

export interface FetchChatsResponse {
  chats: Chat[],
  error: Error | null
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private http: HttpClient, private authService: AuthService) {
  }

  async fetchGlobalChats(): Promise<Chat[]> {
    const headers = {
      Authorization: this.authService.getAuthHeader()
    };

    return this.http.get('/api/v1/chat/global', { headers })
      .pipe(map((rawData) => {
        if (Array.isArray(rawData)) {
          return rawData.map(x => new Chat(x));
        }
        return [];
      }))
      .toPromise();
  }

  async fetchDirectChats(userName: string): Promise<Chat[]> {
    const headers = {
      Authorization: this.authService.getAuthHeader()
    };

    return this.http.get(`/api/v1/chat/direct/${userName}`, { headers })
      .pipe(map((rawData) => {
        if (Array.isArray(rawData)) {
          return rawData.map(x => new Chat(x));
        }
        return [];
      }))
      .toPromise();
  }

  fetchAndSubscribeToChats(mode: ChatMode): Observable<FetchChatsResponse> {
    return timer(0, POLL_MILLIS).pipe(
      flatMap(async () => {
        let chats: Chat[];

        switch (mode.type) {
          case 'global':
            chats = await this.fetchGlobalChats();
            break;
          case 'direct':
            chats = await this.fetchDirectChats(mode.identifier);
            break;
          default:
            throw new Error('not implemented');
        }

        return { chats, error: null };
      }),
      catchError(error => of({ chats: [], error }))
    );
  }

  async sendGlobalChat(message: string): Promise<void> {
    const headers =  {
      Authorization: this.authService.getAuthHeader()
    };

    await this.http.post('/api/v1/chat/global', { message }, { headers }).toPromise();
  }

  async sendDirectChat(userName: string, message: string): Promise<void> {
    const headers =  {
      Authorization: this.authService.getAuthHeader()
    };

    await this.http.post(`/api/v1/chat/direct/${userName}`, { message }, { headers }).toPromise();
  }
}
