import { Post } from '../models/post.model';
import { IPostService } from './post.service';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';

export class LocalStoragePostService implements IPostService {
  constructor(private authService: AuthService, private http: HttpClient) {}

  async fetchFeed(): Promise<Post[]> {
    return Promise.resolve([new Post(), new Post()]);
  }

  async fetchUserFeed(username: string): Promise<Post[]> {
    return Promise.reject();
  }

  async fetchPostById(id: string): Promise<Post | null> {
    return Promise.reject();
  }

  async createPost(post: Post): Promise<void> {
    return Promise.reject();
  }
}
