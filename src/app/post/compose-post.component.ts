import { Component, ViewChild, Inject } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormControl } from '@angular/forms';
import { Post } from '../models/post.model';
import { NgForm } from '@angular/forms';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';

@Component({
  selector: 'app-compose-post',
  templateUrl: './compose-post.component.html',
  styleUrls: ['./compose-post.component.css']
})
export class ComposePostComponent {
  @ViewChild('form') form!: NgForm;
  post: Post = new Post();
  message = ''

  constructor(public authService: AuthService, @Inject(POST_SERVICE_TOKEN) public postService: IPostService) { }

  async submitPost() {
    try {
      await this.postService.createPost(this.post);

      this.message = 'success!';

      this.post = new Post();
      this.form.resetForm();
    } catch(e) {
      this.message = `${e?.message}`;
    }
  }
}
