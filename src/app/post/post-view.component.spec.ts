import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostViewComponent } from './post-view.component';
import {RouterTestingModule} from '@angular/router/testing';
import { routes } from '../app-routing.module';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';

describe('PostViewComponent', () => {
  let component: PostViewComponent;
  let fixture: ComponentFixture<PostViewComponent>;
  let blogServiceStub: Partial<IPostService>;
  let blogService: IPostService;

  beforeEach(async () => {
    blogServiceStub = { fetchFeed: () => Promise.resolve([] as any[]) };
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [ PostViewComponent ],
      providers: [{ provide: POST_SERVICE_TOKEN, useValue: blogServiceStub }]
    })
    .compileComponents();
    blogService = TestBed.inject(POST_SERVICE_TOKEN);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
