import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposePostComponent } from './compose-post.component';
import { AuthService } from '../services/auth.service';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';

describe('ComposePostComponent', () => {
  let component: ComposePostComponent;
  let fixture: ComponentFixture<ComposePostComponent>;

  let authServiceStub: Partial<AuthService>;
  let authService: AuthService;
  let blogServiceStub: Partial<IPostService>;
  let blogService: IPostService;

  describe('when user is not logged in', () => {
    beforeEach(async () => {
      authServiceStub = {
        isLogged: () => false,
        getUserEmail: () => null
      };

      blogServiceStub = {
        createPost: () => Promise.reject()
      }

      await TestBed.configureTestingModule({
        declarations: [ ComposePostComponent ],
        providers: [
          { provide: AuthService, useValue: authServiceStub },
          { provide: POST_SERVICE_TOKEN, useValue: blogServiceStub }
        ]
      })
      .compileComponents();

      authService = TestBed.inject(AuthService);
      blogService = TestBed.inject(POST_SERVICE_TOKEN);
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(ComposePostComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });


    it('renders a link to login page', () => {
      const compiled = fixture.nativeElement;

      expect(compiled.querySelector('a[href="/login"]').textContent).toMatch(/login/i);
    });
  });

  describe('when user is logged in as batman@ehlabs.net', () => {
    beforeEach(async () => {
      authServiceStub = {
        isLogged: () => true,
        getUserEmail: () => 'batman@ehlabs.net'
      };
      blogServiceStub = {
        createPost: () => Promise.resolve()
      }

      await TestBed.configureTestingModule({
        declarations: [ ComposePostComponent ],
        providers: [
          { provide: AuthService, useValue: authServiceStub },
          { provide: POST_SERVICE_TOKEN, useValue: blogServiceStub }
        ]
      })
      .compileComponents();

      authService = TestBed.inject(AuthService);
      blogService = TestBed.inject(POST_SERVICE_TOKEN);
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(ComposePostComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });


    it('renders the composer', () => {
      const compiled = fixture.nativeElement;

      expect(compiled.querySelector('a[href="/login"]')).toBeNull();
      expect(compiled.querySelector('form')).not.toBeNull();
    });
  });
});
