import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../models/post.model';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  @Input() post: Post = new Post();
  @Input() showFull: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  renderBody(): string {
    if (!this.showFull && (this.post.body.length > 200)) {
      return `${this.post.body.substr(0, 200)}...`;
    }
    return this.post.body;
  }
}
