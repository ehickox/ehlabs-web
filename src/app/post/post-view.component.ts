import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../models/post.model';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.css']
})
export class PostViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, @Inject(POST_SERVICE_TOKEN) private postService: IPostService) { }

  isLoading = true
  errorText = ""
  post:Post|null = null

  ngOnInit(): void {
    const postId = this.route.snapshot.paramMap.get('postId');
    if (postId) {
      this.fetchPost(postId);
    }
  }

  async fetchPost(postId: string) {
    try {
      this.post = await this.postService.fetchPostById(postId);
      this.errorText = "";
    }
    catch (e) {
      this.errorText = e.toString();
    }
    finally {
      this.isLoading = false;
    }
  }
}
