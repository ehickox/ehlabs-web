import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostListComponent } from './post-list.component';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('PostListComponent', () => {
  let component: PostListComponent;
  let fixture: ComponentFixture<PostListComponent>;
  let postServiceStub: Partial<IPostService>;
  let postService: IPostService;

  beforeEach(async () => {
    postServiceStub = { fetchFeed: () => Promise.resolve([] as any[]) };
    await TestBed.configureTestingModule({
      declarations: [ PostListComponent ],
      providers: [{ provide: POST_SERVICE_TOKEN, useValue: postServiceStub }],
      imports: [RouterTestingModule.withRoutes([])],
    })
    .compileComponents();
    postService = TestBed.inject(POST_SERVICE_TOKEN);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
