import { Component, Inject } from '@angular/core';
import { Post } from '../models/post.model';
import { ActivatedRoute } from '@angular/router';
import { IPostService, POST_SERVICE_TOKEN } from '../services/post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent {

  constructor(@Inject(POST_SERVICE_TOKEN) private postService: IPostService, private route: ActivatedRoute) {
    this.username = this.route.snapshot.paramMap.get('username') || '';
    this.loadFeed();
  }
  isLoading = true
  errorText = ""
  username = ""

  posts: Post[] = []

  async loadFeed() {
    try {
      if (this.username) {
        this.posts = await this.postService.fetchUserFeed(this.username);
      } else {
        this.posts = await this.postService.fetchFeed();
      }

      this.isLoading = false;
      this.errorText = "";
    } catch (e) {
      this.isLoading = false;
      this.errorText = `${e?.message}`;
    }
  }
}
