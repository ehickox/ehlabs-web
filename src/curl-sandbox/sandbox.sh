# This is a tool for testing the ehlabs api using curl.
#
# instructions: source this file and create environment variables for email and password
#
#  source sandbox.sh
#  EMAIL="batman@ehlabs.com"  # enter your real email here
#  PASS="abc123"              # enter your real password here

#  auth $EMAIL $PASS           # authenticate
# ...server returns { "token": "abasasefawef" }
#
#  TOKEN="abasasefawef"        # paste the real token here
function auth() {
  local email=$1
  local pass=$2

  curl -u ${email}:${pass}  -S "https://www.ehlabs.net/api/v1/auth/token";
}

# check_auth $EMAIL $TOKEN
function check_auth() {
  local email=$1
  local token=$2

  curl -u ${email}:${token} -S "https://www.ehlabs.net/api/v1/auth/check";
}

# create_blogpost $EMAIL $TOKEN 'example title' 'example body'
function create_blogpost() {
  local email=$1
  local token=$2
  local title=$3
  local body=$4

  curl\
    -u ${email}:${token}\
    -H "Response-Type: application/json"\
    -d "title=${title}"\
    -d "body=${body}"\
    --request POST\
    -S "https://www.ehlabs.net/api/v1/blog"
}

function fetch_feed() {
  curl 'https://www.ehlabs.net/api/v1/blog/feed';
}

function fetch_user_feed() {
  local username=$1

  curl "https://www.ehlabs.net/api/v1/blog/u/${username}"
}

function fetch_global_chats() {
  local email=$1
  local token=$2

  curl\
    -u ${email}:${token}\
    -H "Response-Type: application/json"\
    --request GET\
    -S "https://www.ehlabs.net/api/v1/chat/global"
}

function fetch_global_chat_stream() {
  local email=$1
  local token=$2

  curl -H "Authorization: Basic: $(echo $email:$token | base64)"\
    -H "Response-Type: application/json"\
    --request GET\
    -S "https://www.ehlabs.net/api/v1/chat/stream/global"
}

function send_global_chat() {
  local email=$1
  local token=$2
  local message=$3

  curl\
    -u "${EMAIL}:${TOKEN}"\
    -H "Response-Type: application/json"\
    --request POST\
    -d "message=${message}"\
    -S "https://www.ehlabs.net/api/v1/chat/global"
}

function fetch_users() {
  local email=$1
  local token=$2

  curl\
    -u "${EMAIL}:${TOKEN}"\
    -H "Response-Type: application/json"\
    --request GET\
    -S "https://www.ehlabs.net/api/v1/users"
}
